package info.vetrov.test;

import javax.swing.*;

import com.sun.star.beans.*;
import com.sun.star.util.*;
import com.sun.star.io.IOException;
import com.sun.star.lang.*;
import com.sun.star.frame.*;
import com.sun.star.util.CloseVetoException;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;

import ooo.connector.BootstrapSocketConnector;

import com.sun.star.sheet.*;
import com.sun.star.table.*;

public class Calc
{   
    private XComponent xComponent = null;
    private XMultiComponentFactory xServiceManager = null;
    private XComponentContext xContext = null;
 
    /**
     * В конструкторе класса соединяемся с работающим экземпляром OpenOffice.org
     * и получаем менеджер сервисов
     * @param String programsURL - путь до директории, в которой находятся
     * исполняемые файлы OpenOffice.org, а именно файл <i>soffice</i> или <i>soffice.exe</i>
     */
    Calc(String programsURL)
    {
        try
        {   
            //получаем компонентный контекст
            xContext = BootstrapSocketConnector.bootstrap(programsURL);
            //получаем менеджер сервисов
            xServiceManager = xContext.getServiceManager();       
        }
        catch (com.sun.star.comp.helper.BootstrapException e)
        {
            e.printStackTrace();
        }
    }
 
    /**
     * Метод осуществляет открытие документа OpenOffice.org Calc
     * @param String docURL - путь до открываемого файла
     */
    public void openDocument(String docURL)
    {
        try
        {
            //создание экземпляра объекта рабочего стола
            Object oDesktop = xServiceManager.createInstanceWithContext("com.sun.star.frame.Desktop", xContext);       
            //приведением типов получаем объект загрузчика компонентов
            XComponentLoader xCompLoader = (XComponentLoader)UnoRuntime.queryInterface(XComponentLoader.class, oDesktop);
 
            java.io.File sourceFile = new java.io.File(docURL);
 
            StringBuffer sbTmp = new StringBuffer("file:///");
 
            sbTmp.append(sourceFile.getCanonicalPath().replace('\\', '/'));
            docURL = sbTmp.toString();
 
            //создаем компонент-лист
 
            xComponent = xCompLoader.loadComponentFromURL( docURL, "_blank", 0, new PropertyValue[0]);
        }
        catch (java.io.IOException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
 
    /**
     * Сохранение открытого документа под своим именем
     */
    public void saveDocument()
    {
        XStorable store = (XStorable)UnoRuntime.queryInterface(XStorable.class, xComponent);
 
        try
        {
            store.storeAsURL(store.getLocation(), new PropertyValue[0]);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
     /**
     * Закрытие документа
     */
    public void closeDocument()
      {
          XCloseable xCloseable = (XCloseable)UnoRuntime.queryInterface(XCloseable.class, xComponent);
 
          try
          {
            xCloseable.close(false);
          }
          catch (CloseVetoException e)
          {
            e.printStackTrace();
          }
      }
 
    /**
     * Создание закладки, перезапись существующей
     */
    public void insertValueIntoCellByName(String pageName, int top, int rows)
    {
        try
        {
            //создаем документ-лист (образуется запросом интерфейса и приведением типа)
            XSpreadsheetDocument xSpreadsheetDocument=(XSpreadsheetDocument)UnoRuntime.queryInterface(XSpreadsheetDocument.class, xComponent);
 
            //получаем набор листов документа
            XSpreadsheets xSpreadsheets=xSpreadsheetDocument.getSheets();
            //и добавляем страницу
            try {
            	xSpreadsheets.removeByName(pageName);
			} catch (Exception e) {
				System.out.println("Сreate new page");
			}
            xSpreadsheets.insertNewByName(pageName, (short)0);
            //получаем объект-лист типа Object
            Object sheet=xSpreadsheets.getByName(pageName);
            //и приводим его к нужному типу
            XSpreadsheet xSpreadsheet=(XSpreadsheet)UnoRuntime.queryInterface(XSpreadsheet.class, sheet);

            //треугольник Паскаля. Вершина
            String cellName;
            cellName =getCharForNumber(rows+1)+1;
            //выделяем ячейку cellName
            XCellRange xCellRange = xSpreadsheet.getCellRangeByName(cellName+":"+cellName);
            //получаем объект ячейка
            XCell xCell = xCellRange.getCellByPosition(0, 0);
            //вставляем в ячейку строку
            xCell.setValue(top);            
            
            //треугольник Паскаля. Тело
            for (int i = 2; i <= rows; i++) {
            	for (int j = 2; j <= rows*2; j++) {
            		//определяем редактируемую ячейку
            		cellName =getCharForNumber(j)+i;
                    xCellRange = xSpreadsheet.getCellRangeByName(cellName+":"+cellName);
                    xCell = xCellRange.getCellByPosition(0, 0);
                    //вставляем в ячейку сумму двух верхник строк
                    xCell.setFormula(value(pageName, getCharForNumber(j-1)+(i-1), getCharForNumber(j+1)+(i-1))); 
				}
			}
        }
        catch(Exception e)
        {
        	 e.printStackTrace();
        }
    }
 
    /**
     * Перевод цифрового значения в текстовое
     * @param int i - порядковый номер буквы в алфавите
     */
    private String getCharForNumber(int i) {

    	String s;
    	
    	if (i > 0 && i < 27) {
    		s = String.valueOf((char)(i + 64));
		} else {
			
			s = "A"+String.valueOf((char)(i + 38));
		}
    	return s; 
    }
    
    /**
     * Определение значения ячейки таблицы
     * @param String pageName - имя страницы
     * @param String cellNameLeft - имя верхней левой ячейки 
     * @param cellName String cellNameRight - имя верхней правой ячейки
     */
    private String value(String pageName, String cellNameLeft, String cellNameRight) {
    	//Значение верхней левой ячейки
    	int valueL;
    	if (getTextFromCellByName(pageName, cellNameLeft).isEmpty()) {
    		valueL=0;
		} else {
			valueL=Integer.parseInt(getTextFromCellByName(pageName, cellNameLeft));
		}
    	
    	//Значение верхней правой ячейки
    	int valueR;
    	if (getTextFromCellByName(pageName, cellNameRight).isEmpty()) {
    		valueR=0;
		} else {
			valueR=Integer.parseInt(getTextFromCellByName(pageName, cellNameRight));
		}
    	
    	//Суммируем значения
    	int value;
    	value =valueL+valueR;
    	
    	//Меняем тип переменной
    	String v;
    	if (value==0) {
			v="";
		} else {
			v=Integer.toString(value);
		}
    	
    	return v;
    }
    
    /**
     * Сохранение открытого файла под именем <code>saveURL</code>
     * @param String saveURL - имя файла для сохранения
     */
    public void saveDocumentAs(String saveURL)
    {
        String convertedURL = null;
        XStorable store = (XStorable)UnoRuntime.queryInterface(XStorable.class, xComponent);
 
        //Преобразование пути
        StringBuffer sbTmp = new StringBuffer("file:///");
        sbTmp.append(saveURL.replace('\\', '/'));
        convertedURL = sbTmp.toString();
 
        try
        {
            store.storeAsURL(convertedURL, new PropertyValue[0]);
        }
        catch (Exception e)
        {       
            JLabel lb = new JLabel("<center>Невозможно сохранить файл <i>"+saveURL+"</i></center>");
            JOptionPane.showMessageDialog(null, lb, "Ошибка!",JOptionPane.ERROR_MESSAGE);
        }
    }
 
    /**
     * Метод получает текст <code>text</code> на странице <code>pageName</code>
     * из ячейки с именем <code>cellName</code>. 
     *
     * @param String pageName - имя страницы
     * @param String cellName - имя ячейки
     */
    public String getTextFromCellByName(String pageName, String cellName)
    {
    	String res="";
        try
        {
            //создаем документ-лист (образуется запросом интерфейса и приведением типа)
            XSpreadsheetDocument xSpreadsheetDocument=(XSpreadsheetDocument)UnoRuntime.queryInterface(XSpreadsheetDocument.class, xComponent);
     
            //получаем набор листов документа
            XSpreadsheets xSpreadsheets=xSpreadsheetDocument.getSheets();
     
            //получаем объект-лист типа Object
            Object sheet=xSpreadsheets.getByName(pageName);
            //и приводим его к нужному типу
            XSpreadsheet xSpreadsheet=(XSpreadsheet)UnoRuntime.queryInterface(XSpreadsheet.class, sheet);
     
            //выделяем ячейку cellName
            XCellRange xCellRange = xSpreadsheet.getCellRangeByName(cellName+":"+cellName);
            //получаем объект ячейка
            XCell xCell = xCellRange.getCellByPosition(0, 0);
            //Беру данные из ячейки
            com.sun.star.text.XText xCellText = 
                (com.sun.star.text.XText) UnoRuntime.queryInterface( com.sun.star.text.XText.class, xCell );
				res = xCellText.getString();	
        }
        catch(Exception e)
        {
            
        }
        return res;
    }
    
    public static void main(String[] args){
    	//директория OpenOffice.org
    	String appUrl = "/Applications/OpenOffice.app/Contents/MacOS";
    	//директория с документом
    	String docUrl = "./lib/"+args[0];
    	//имя стриницы в документе
    	String cellName = "Pascal's Triangle";
    	//значение вершины треугольника
    	int triangleTop = 1;
    	//колличество строк в треугольнике (max 20)
    	int count = 20;	

    	Calc calcDocumentnew = new Calc(appUrl);
//Open
    	calcDocumentnew.openDocument(docUrl);
//Edit
    	calcDocumentnew.insertValueIntoCellByName(cellName, triangleTop, count);
//Save
    	calcDocumentnew.saveDocument();
//Close
    	calcDocumentnew.closeDocument();
    }
}
